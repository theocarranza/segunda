from django.apps import AppConfig


class EvernoteConfig(AppConfig):
    name = 'evernote'
