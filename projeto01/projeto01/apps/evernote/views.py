from django.shortcuts import render, get_object_or_404
from .models import Memory
from .forms import MemoryForm

# Create your views here.
def evernote_list(request):
    return render(request, 'evernote.html', {'memory':Memory.objects.all()})
